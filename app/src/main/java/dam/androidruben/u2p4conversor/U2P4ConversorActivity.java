package dam.androidruben.u2p4conversor;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class U2P4ConversorActivity extends LogActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        EditText etPulgada = findViewById(R.id.et_Pulgada);
        EditText etResultado = findViewById(R.id.et_Resultado);
        Button buttonConvertir = findViewById(R.id.button_Convertir);
        TextView textViewError = findViewById(R.id.textViewError);

        buttonConvertir.setOnClickListener(view -> {
            try {
                etResultado.setText(convertirPulgadas(etPulgada.getText().toString()));
                Log.i("LogsConversor","botón convertir1");
            } catch (Exception e) {
                textViewError.setText(e.getMessage());
                Log.e("LogsConversor", e.getMessage());
            }
        });

        EditText etCentimetro = findViewById(R.id.et_Centimetro);
        EditText etResultado2 = findViewById(R.id.et_Resultado2);
        Button buttonConvertir2 = findViewById(R.id.button_Convertir2);

        buttonConvertir2.setOnClickListener(view -> {
            try {
                etResultado2.setText(convertirCentimetros(etCentimetro.getText().toString()));
                Log.i("LogsConversor","botón convertir2");
            } catch (Exception e) {
                textViewError.setText(e.getMessage());
                Log.e("LogsConversor", e.getMessage());
            }
        });
    }

    private String convertirPulgadas(String pulgadaText) throws Exception {
        if (Double.parseDouble(pulgadaText) < 1) {
            throw new Exception("Sólo números >=1");
        }
        double pulgadaValue = Double.parseDouble(pulgadaText) * 2.54;
        return String.valueOf(pulgadaValue);
    }

    private String convertirCentimetros(String centimetroText) throws Exception {
        if (Double.parseDouble(centimetroText) < 1) {
            throw new Exception("Sólo números >=1");
        }
        double centimetroValue = Double.parseDouble(centimetroText) / 2.54;
        DecimalFormat df = new DecimalFormat("#.00");
        return String.valueOf(df.format(centimetroValue));
    }
}